import React from 'react';
import Menu from '../Menu';
import ListagemPessoas from './ListagemPessoas';

export default class Pessoas extends React.Component{

  constructor(props){
    super(props)
  }

  render(){
    return(
      <div>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"></link>
      <Menu></Menu>
      <ListagemPessoas></ListagemPessoas> 
    </div>
    );
  }
}

