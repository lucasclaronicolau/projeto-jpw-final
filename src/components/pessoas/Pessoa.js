import React from 'react';
import axios from 'axios';
import Menu from '../Menu';
import moment from 'moment';
import { Link } from 'react-router-dom'


export default class Pessoa extends React.Component{

    constructor(props){
        super(props)
        this.state={
            nome: '',
            end: '',
            nasc: '',
            treinador: ''
        }
        this.BASE_URL="http://localhost:5000/pessoas/";
    }
        
    componentDidMount = () =>{
        if(this.props.match.params.id !== 'adicionar'){
            this.loadPessoa()
        }else{
            this.setState({
                treinador: false,
                nasc: Date.now(),
            })
        }
    }

    loadPessoa = () => {
        axios.get(this.BASE_URL +this.props.match.params.id,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                nome: response.data.nome,
                end: response.data.end,
                nasc: response.data.nasc,
                trein: response.data.trein
            })
        }).catch((error) => {
            if (error.response) {
                if(error.response.status === 404){
                    document.getElementById('404').hidden = false;
                }else{
                    document.getElementById('500').hidden = false;
                }
            }
        })
    }

    savePessoa = () =>{
        var dados = {
            nome: this.state.nome,
            end: this.state.end,
            nasc: this.state.nasc,
            trein: this.state.treinador
        }
        console.log(dados)
        if(this.props.match.params.id !== 'adicionar'){
            axios.put(this.BASE_URL +this.props.match.params.id, dados ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
            
        }else{
            axios.post(this.BASE_URL, dados ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
        }
      this.props.history.push('/pessoas')
    }

    changeNome = (event) =>{
        this.setState({ 
            nome: event.target.value
        })
    }

    changeEnd = (event) =>{
        this.setState({ 
            end: event.target.value
        })
    }
    
    changeNasc = (event) =>{
        this.setState({ 
            nasc: event.target.value
        })
    }

    changeTrein = (event) =>{
        this.setState({ 
            trein: event.target.checked
        })
    }

    render(){
        return (
            <div>
                <Menu></Menu>
                <div id="404" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Pessoa não encontrado.
                </div>
                <div id="500" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Erro interno.
                </div>
                <div className="container-fluid">
                    <br/>
                    <h3>Pessoa</h3>
                    <br/>
                    <form onSubmit={this.savePessoa}>
                        <div className="container-fluid">
                            <label>Nome *</label>
                            <input type="text" className="form-control" id="nome" placeholder="Nome" onChange={this.changeNome} value={this.state.nome} required/>
                            <label>Endereço *</label>
                            <input type="text" className="form-control" id="end" placeholder="Endereço" onChange={this.changeEnd} value={this.state.end} required/>
                            <label>Data de Nascimento *</label>
                            <input type="date" className="form-control" id="nasc" placeholder="Data de Nacimento" onChange={this.changeNasc} value ={moment(this.state.nasc).format('YYYY-MM-DD')} required/>
                            <br/>
                            <div className="form-check">
                                <input className="form-check-input" type="checkbox" value={this.state.treinador} onChange={this.changeTrein} id="trein"/>
                                <label className="form-check-label">Treinador</label>
                            </div>
                            <label>* Campo Obrigatório</label>
                        </div>
                        <br/>
                        <div className="container-fluid">
                            <div className="form-group row">
                                <div className="col-sm-2">
                                    <button className="btn btn-primary form-control" type="submit">Salvar</button>
                                </div>
                                <div className="col-sm-2">
                                    <Link className="btn btn-primary form-control" to="/pessoas">Cancelar</Link>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}