import React from 'react';
import axios from 'axios';
import GridPessoas from './GridPessoas';

export default class ListagemPessoas extends React.Component{

    constructor(props){
        super(props)
        this.state={
            pessoas: []
        }
        this.BASE_URL="http://localhost:5000"        
    }

    componentDidMount = ()  => {
        this.loadPessoas()
    }

    loadPessoas = () => {
        axios.get(this.BASE_URL+'/pessoas',{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                pessoas: response.data
            })
        }).catch((error) => {
            console.error(error)
        })
    }
        
    render(){
        this.componentDidMount()
        return(
            <div className="container-fluid">
                <br/>
                <h3>Listagem de Pessoas</h3>
                <br/>
                <GridPessoas pessoas={this.state.pessoas}></GridPessoas>
            </div>
        )
    }
}