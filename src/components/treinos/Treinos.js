import React from 'react';
import Menu from '../Menu';
import ListagemTreinos from './ListagemTreinos';

export default class Treinos extends React.Component{

  constructor(props){
    super(props)
  }

  render(){
    return(
      <div>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"></link>
      <Menu></Menu>
      <ListagemTreinos></ListagemTreinos> 
    </div>
    );
  }
}

