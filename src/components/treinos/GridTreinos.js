import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class GridTreinos extends React.Component{

    constructor(props){
        super(props)
        this.BASE_URL="http://localhost:5000/treinos/";
    }

    deleteTreino = (data) =>{
        axios.delete(this.BASE_URL +data,{headers: {authorization: "Lucas"}}).then(res => {
            console.log(res);
            console.log(res.data);
        })
    }

        
    render(){
        return (
            <div>
                <div className="container-fluid">
                    <div className="row nav">
                        <Link className="nav-link" to="/treinos/adicionar">ADICIONAR</Link>
                    </div>
                    <div className="row">
                        <div className="col col-1 font-weight-bold">Ações</div> 
                        <div className="col font-weight-bold">Nome</div>    
                        <div className="col font-weight-bold">Treinador</div>
                    </div>
                    {this.props.treinos.map((item) =>{
                        return<div className="row" key={item._id}>
                                <div className="col col-1 dados">
                                <Link to={'/treinos/'+item._id}>
                                    <span className="material-icons">edit</span>    
                                </Link>
                                <a href="">
                                    <span className="material-icons" onClick={this.deleteTreino.bind(this, item._id)}>delete</span>
                                </a>
                                </div>    
                                <div className="col dados">{item.nome}</div>    
                                <div className="col dados">{item.treinador.nome}</div>
                            </div>
                    })}
                </div>
            </div>
        )
    }
}