import React from 'react';
import axios from 'axios';
import GridTreinos from './GridTreinos';

export default class ListagemTreinos extends React.Component{

    constructor(props){
        super(props)
        this.state={
            treinos: []
        }
        this.BASE_URL="http://localhost:5000"        
    }

    componentDidMount = ()  => {
        this.loadTreinos()
    }

    loadTreinos = () => {
        axios.get(this.BASE_URL+'/treinos',{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                treinos: response.data
            })
        }).catch((error) => {
            console.error(error)
        })
    }
        
    render(){
        this.componentDidMount()
        return(
            <div className="container-fluid">
                <br/>
                <h3>Listagem de Treinos</h3>
                <br/>
                <GridTreinos treinos={this.state.treinos}></GridTreinos>
            </div>
        )
    }
}