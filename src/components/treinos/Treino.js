import React from 'react';
import axios from 'axios';
import Menu from '../Menu';
import { Link } from 'react-router-dom'


export default class Treino extends React.Component{

    constructor(props){
        super(props)
        this.state={
            nome: '',
            treinador: '',
            alunos: [],
            equipamentos: [],
            equipamentosList: [],
            pessoas: []
        }
        this.BASE_URL="http://localhost:5000/treinos/";
    }
        
    componentDidMount = () =>{
        this.loadPessoas()   
        this.loadEquipamentos()
        if(this.props.match.params.id !== 'adicionar'){
            this.loadTreino()
        }
    }

    loadPessoas = () => {
        axios.get("http://localhost:5000/pessoas" ,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                pessoas: response.data
            })
        }).catch((error) => {
            document.getElementById('500').hidden = false;
        })
    }

    loadEquipamentos = () =>{
        axios.get("http://localhost:5000/equipamentos" ,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                equipamentosList: response.data
            })
        }).catch((error) => {
            document.getElementById('500').hidden = false;
        })
    }

    loadTreino = () => {
        axios.get(this.BASE_URL +this.props.match.params.id,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                nome: response.data.nome,
                alunos: response.data.alunos,
                treinador: response.data.treinador,
                equipamentos: response.data.equip
            })
        }).catch((error) => {
            if (error.response) {
                if(error.response.status === 404){
                    document.getElementById('404').hidden = false;
                }else{
                    document.getElementById('500').hidden = false;
                }
            }
        })
    }

    saveTreino = () =>{
        var dados = {
            alunos: this.state.alunos,
            equip: this.state.equipamentos,
            nome: this.state.nome,
            treinador: this.state.treinador
        }
        console.log(dados)
        if(this.props.match.params.id !== 'adicionar'){
            axios.put(this.BASE_URL +this.props.match.params.id, dados ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
            
        }else{
            axios.post(this.BASE_URL, dados ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
        }
      this.props.history.push('/treinos')
    }

    changeAlunos = (event) =>{
        var options = event.target.options;
        var value = [];
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }
        this.setState({
            alunos: value
        })
    }

    changeTreinador = (event) =>{
        this.setState({ 
            treinador: event.target.value
        })
    }

    changeEquipamentos = (event) =>{
        var options = event.target.options;
        var value = [];
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }
        this.setState({
            equipamentos: value
        })
    }

    changeNome = (event) =>{
        this.setState({ 
            nome: event.target.value
        })
    }
    
    render(){
        return (
            <div>
                <Menu></Menu>
                <div id="404" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Equipamento não encontrado.
                </div>
                <div id="500" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Erro interno.
                </div>
                <div className="container-fluid">
                    <br/>
                    <h3>Treino</h3>
                    <br/>
                    <form onSubmit={this.saveTreino}>
                        <div className="container-fluid">
                            <label>Nome *</label>
                            <input type="text" className="form-control" id="nome" placeholder="Nome" onChange={this.changeNome} value={this.state.nome} required/>
                            <label>Treinador *</label>
                            <select className="form-control" id="treinador" placeholder="Treinador" onChange={this.changeTreinador} required>
                            <option disabled selected hidden></option>
                                {this.state.pessoas.map((item) =>{
                                    if(item.trein == true){
                                        if(item._id == this.state.treinador){
                                            return <option key={item._id} value={item._id} selected>{item.nome}</option>    
                                        }else{
                                            return <option key={item._id} value={item._id}>{item.nome}</option>
                                        }
                                    }
                                })}
                            </select>
                            <label>Alunos *</label>
                            <select className="form-control" id="alunos" placeholder="Alunos" onChange={this.changeAlunos} required multiple>
                                {this.state.pessoas.map((item) => {                                     
                                    if(item.trein != true){
                                        if(this.state.alunos.includes(item._id)){
                                            return <option key={item._id} value={item._id} selected>{item.nome}</option>    
                                        }else{
                                            return <option key={item._id} value={item._id}>{item.nome}</option>
                                        }
                                    }
                                })}
                            </select>
                            <label>Equipamentos *</label>
                            <select className="form-control" id="equipamentos" placeholder="Equipamentos" onChange={this.changeEquipamentos} required multiple>
                                {this.state.equipamentosList.map((item) =>{
                                    if(this.state.equipamentos.includes(item._id)){
                                        return <option key={item._id} value={item._id} selected>{item.nome}</option>
                                    }else{
                                        return <option key={item._id} value={item._id}>{item.nome}</option>
                                    }
                                })}
                            </select>
                            <label>* Campo Obrigatório</label>
                        </div>
                        <br/>
                        <div className="container-fluid">
                            <div className="form-group row">
                                <div className="col-sm-2">
                                    <button className="btn btn-primary form-control" type="submit">Salvar</button>
                                </div>
                                <div className="col-sm-2">
                                    <Link className="btn btn-primary form-control" to="/treinos">Cancelar</Link>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}