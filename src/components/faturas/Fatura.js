import React from 'react';
import axios from 'axios';
import Menu from '../Menu';
import moment from 'moment';
import { Link } from 'react-router-dom'


export default class Local extends React.Component{

    constructor(props){
        super(props)
        this.state={
            pessoa: '',
            issuedate: '',
            duedate: '',
            pessoas: []
        }
        this.BASE_URL="http://localhost:5000/faturas/";
    }
        
    componentDidMount = () =>{
        this.loadPessoas()   
        if(this.props.match.params.id !== 'adicionar'){
            this.loadFatura()
        }else{
            this.setState({
                issuedate: Date.now(),
                duedate: Date.now()
            })
        }
    }

    loadPessoas = () => {
        axios.get("http://localhost:5000/pessoas" ,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                pessoas: response.data
            })
        }).catch((error) => {
            document.getElementById('500').hidden = false;
        })
    }

    loadFatura = () => {
        axios.get(this.BASE_URL +this.props.match.params.id,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                pessoa: response.data.pessoa._id,
                issuedate: response.data.issuedate,
                duedate: response.data.duedate
            })
        }).catch((error) => {
            if (error.response) {
                if(error.response.status === 404){
                    document.getElementById('404').hidden = false;
                }else{
                    document.getElementById('500').hidden = false;
                }
            }
        })
    }

    saveFatura = () =>{
        var dados = {
            pessoa: this.state.pessoa,
            issuedate: this.state.issuedate,
            duedate: this.state.duedate,
        }
        if(this.props.match.params.id !== 'adicionar'){
            axios.put(this.BASE_URL +this.props.match.params.id, dados ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
            
        }else{
            axios.post(this.BASE_URL, dados ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
        }
      this.props.history.push('/faturas')
    }

    changePessoa = (event) =>{
        this.setState({ 
            pessoa: event.target.value
        })
    }

    changeIssuedate = (event) =>{
        this.setState({ 
            issuedate: event.target.value
        })
    }

    changeDuedate = (event) =>{
        this.setState({ 
            duedate: event.target.value
        })
    }

    render(){
        return (
            <div>
                <Menu></Menu>
                <div id="404" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Fatura não encontrado.
                </div>
                <div id="500" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Erro interno.
                </div>
                <div className="container-fluid">
                    <br/>
                    <h3>Local</h3>
                    <br/>
                    <form onSubmit={this.saveFatura}>
                        <div className="container-fluid">
                            <label>Pessoa *</label>
                            <select className="form-control" id="pessoa" placeholder="Nome" onSelect={this.changePessoa} required>
                            <option disabled selected hidden></option>
                                {this.state.pessoas.map((item) =>{
                                    if(item.trein != true){
                                        if(item._id == this.state.pessoa){
                                            return <option key={item._id} value={item._id} selected>{item.nome}</option>    
                                        }else{
                                            return <option key={item._id} value={item._id}>{item.nome}</option>
                                        }
                                    }
                                })}
                            </select>
                            <label>Data de Emissão *</label>{console.log(moment(this.state.issuedate).format('YYYY-MM-DD'))}
                            <input type="date" className="form-control" id="issuedate" placeholder="Data de Emissão" onChange={this.changeIssuedate} value ={moment(this.state.issuedate).format('YYYY-MM-DD')}/>
                            <label>Data de Vencimento *</label>
                            <input type="date" className="form-control" id="duedate" placeholder="Data de Vencimento" onChange={this.changeDuedate} value ={moment(this.state.duedate).format('YYYY-MM-DD')} required/>
                            <label>* Campo Obrigatório</label>
                        </div>
                        <br/>
                        <div className="container-fluid">
                            <div className="form-group row">
                                <div className="col-sm-2">
                                    <button className="btn btn-primary form-control" type="submit">Salvar</button>
                                </div>
                                <div className="col-sm-2">
                                    <Link className="btn btn-primary form-control" to="/faturas">Cancelar</Link>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}