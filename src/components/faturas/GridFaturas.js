import React from 'react';
import axios from 'axios';
import moment from 'moment';
import { Link } from 'react-router-dom';

export default class GridLocais extends React.Component{

    constructor(props){
        super(props)
        this.BASE_URL="http://localhost:5000/faturas/";
    }

    deleteLocal = (data) =>{
        axios.delete(this.BASE_URL +data,{headers: {authorization: "Lucas"}}).then(res => {
            console.log(res);
            console.log(res.data);
        })
    }

        
    render(){
        return (
            <div>
                <div className="container-fluid">
                    <div className="row nav">
                        <Link className="nav-link" to="/faturas/adicionar">ADICIONAR</Link>
                    </div>
                    <div className="row">
                        <div className="col col-1 font-weight-bold">Ações</div> 
                        <div className="col font-weight-bold">Pessoa</div>    
                        <div className="col font-weight-bold">Data de Emissão</div>
                        <div className="col font-weight-bold">Data de Vencimentos</div>
                    </div>
                    {this.props.faturas.map((item) =>{
                        return<div className="row" key={item._id}>
                                <div className="col col-1 dados">
                                <Link to={'/faturas/'+item._id}>
                                    <span className="material-icons">edit</span>    
                                </Link>
                                <a href="">
                                    <span className="material-icons" onClick={this.deleteLocal.bind(this, item._id)}>delete</span>
                                </a>
                                </div>    
                                <div className="col dados">{item.pessoa.nome}</div>    
                                <div className="col dados">{moment(new Date(item.issuedate)).format("DD/MM/YYYY")}</div>
                                <div className="col dados">{moment(new Date(item.duedate)).format("DD/MM/YYYY")}</div>    
                            </div>
                    })}
                </div>
            </div>
        )
    }
}