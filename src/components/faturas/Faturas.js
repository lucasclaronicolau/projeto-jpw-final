import React from 'react';
import Menu from '../Menu';
import ListagemFaturas from './ListagemFaturas';

export default class Faturas extends React.Component{

  constructor(props){
    super(props)
  }

  render(){
    return(
      <div>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"></link>
      <Menu></Menu>
      <ListagemFaturas></ListagemFaturas> 
    </div>
    );
  }
}

