import React from 'react';
import axios from 'axios';
import GridFaturas from './GridFaturas';

export default class ListagemFaturas extends React.Component{

    constructor(props){
        super(props)
        this.state={
            faturas: []
        }
        this.BASE_URL="http://localhost:5000"        
    }

    componentDidMount = ()  => {
        this.loadFaturas()
    }

    loadFaturas = () => {
        axios.get(this.BASE_URL+'/faturas',{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                faturas: response.data
            })
        }).catch((error) => {
            console.error(error)
        })
    }
        
    render(){
        this.componentDidMount()
        return(
            <div className="container-fluid">
                <br/>
                <h3>Listagem de Faturas</h3>
                <br/>
                <GridFaturas faturas={this.state.faturas}></GridFaturas>
            </div>
        )
    }
}