import React from 'react';
import { Link } from 'react-router-dom'

export default class Menu extends React.Component{

    render(){
        return (
            <nav className="navbar navbar-expand-lg navbar-light nav">
                <a className="navbar-brand" href="#">
                    <img src="/logo.png" width="30" height="30" className="d-inline-block align-top"></img>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                    <span className="navbar-toggler-icon"></span>
                </button>                
                <div className="collapse navbar-collapse" id="conteudoNavbarSuportado">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/pessoas">Pessoas</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/equipamentos">Equipamentos</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/faturas">Faturas</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/locais">Locais</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/treinos">Treinos</Link>
                        </li>
                    </ul>
                </div>
            </nav>
            
        )
    }

}
