import React from 'react';
import Menu from '../Menu';
import ListagemEquipamentos from './ListagemEquipamentos';

export default class Equipamentos extends React.Component{

  constructor(props){
    super(props)
  }

  render(){
    return (
      <div>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
        rel="stylesheet"></link>
        <Menu></Menu>
        <ListagemEquipamentos></ListagemEquipamentos> 
      </div>
    );
  
  }
}




