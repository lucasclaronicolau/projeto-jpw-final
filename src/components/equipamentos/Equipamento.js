import React from 'react';
import axios from 'axios';
import Menu from '../Menu';
import { Link } from 'react-router-dom'


export default class Equipamento extends React.Component{

    constructor(props){
        super(props)
        this.state={
            nome: '',
            qnt: ''
        }
        this.BASE_URL="http://localhost:5000/equipamentos/";
    }
        
    componentDidMount = () =>{
        console.log(this.props.match.params.id)
        if(this.props.match.params.id !== 'adicionar'){
            this.loadEquipamento()
        }
    }

    loadEquipamento = () => {
        axios.get(this.BASE_URL +this.props.match.params.id,{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                nome: response.data.nome,
                qnt: response.data.qnt,
            })
            console.log(response)
        }).catch((error) => {
            if (error.response) {
                if(error.response.status === 404){
                    document.getElementById('404').hidden = false;
                }else{
                    document.getElementById('500').hidden = false;
                }
            }
        })
    }

    saveEquipamento = () =>{
        if(this.props.match.params.id !== 'adicionar'){
            axios.put(this.BASE_URL +this.props.match.params.id, this.state ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
            
        }else{
            axios.post(this.BASE_URL, this.state ,{headers: {authorization: "Lucas"}}).then(res => {
                console.log('ok');
            })
        }
      this.props.history.push('/equipamentos')
    }

    changeName = (event) =>{
        this.setState({ 
            nome: event.target.value
        })
    }

    changeQnt = (event) =>{
        this.setState({ 
            qnt: event.target.value
        })
    }

    render(){
        return (
            <div>
                <Menu></Menu>
                <div id="404" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Equipamento não encontrado.
                </div>
                <div id="500" className="alert alert-danger alert-dismissible" hidden>
                    <a href="" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erro!</strong> Erro interno.
                </div>
                <div className="container-fluid">
                    <br/>
                    <h3>Equipamento</h3>
                    <br/>
                    <form onSubmit={this.saveEquipamento}>
                        <div className="container-fluid">
                            <label>Nome *</label>
                            <input type="text" className="form-control" id="nome" placeholder="Nome" onChange={this.changeName} value={this.state.nome} required/>
                            <label>Quantidade *</label>
                            <input type="number" className="form-control" id="qnt" placeholder="Quantidade" onChange={this.changeQnt} value={this.state.qnt} required/>
                            <label>* Campo Obrigatório</label>
                        </div>
                        <br/>
                        <div className="container-fluid">
                            <div className="form-group row">
                                <div className="col-sm-2">
                                    <button className="btn btn-primary form-control" type="submit">Salvar</button>
                                </div>
                                <div className="col-sm-2">
                                    <Link className="btn btn-primary form-control" to="/equipamentos">Cancelar</Link>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}