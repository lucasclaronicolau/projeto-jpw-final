import React from 'react';
import axios from 'axios';
import GridEquipamentos from './GridEquipamentos';

export default class ListagemEquipamentos extends React.Component{

    constructor(props){
        super(props)
        this.state={
            equipamentos: []
        }
        this.BASE_URL="http://localhost:5000"
    }

    componentDidMount = () =>{
        this.loadEquipamentos()
    }

    loadEquipamentos = () => {
        axios.get(this.BASE_URL+'/equipamentos',{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                equipamentos: response.data
            })
        }).catch((error) => {
            console.error(error)
        })
    }
        
    render(){
        this.componentDidMount()
        return(
            <div className="container-fluid">
                <br/>
                <h3>Listagem de Equipamentos</h3>
                <br/>
                <GridEquipamentos equipamentos={this.state.equipamentos}></GridEquipamentos>
            </div>
        )
    }
}