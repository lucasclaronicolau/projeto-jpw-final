import React from 'react';
import axios from 'axios';
import GridEquipamentos from './GridLocais';

export default class ListagemLocais extends React.Component{

    constructor(props){
        super(props)
        this.state={
            locais: []
        }
        this.BASE_URL="http://localhost:5000"
    }

    componentDidMount = () =>{
        this.loadLocais()
    }

    loadLocais = () => {
        axios.get(this.BASE_URL+'/locais',{headers: {authorization: "Lucas"}}).then((response) => {
            this.setState({
                locais: response.data
            })
        }).catch((error) => {
            console.error(error)
        })
    }
        
    render(){
        this.componentDidMount()
        return(
            <div className="container-fluid">
                <br/>
                <h3>Listagem de Locais</h3>
                <br/>
                <GridEquipamentos locais={this.state.locais}></GridEquipamentos>
            </div>
        )
    }
}