import React from 'react';
import Menu from '../Menu';
import ListagemLocais from './ListagemLocais';

export default class Locais extends React.Component{

  constructor(props){
    super(props)
  }

  render(){
    return(
      <div>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"></link>
      <Menu></Menu>
      <ListagemLocais></ListagemLocais> 
    </div>
    );
  }
}

