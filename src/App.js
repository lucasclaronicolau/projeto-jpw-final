import React from 'react';
import Menu from './components/Menu';
import Card from './components/Card';

function App() {
  return (
    <div>
      <Menu></Menu>
      <Card></Card>
    </div>
  );
}

export default App;
