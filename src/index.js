import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Equipamentos from './components/equipamentos/Equipamentos';
import AdicionarEquipamentos from './components/equipamentos/Equipamento';
import Locais from './components/locais/Locais';
import AdicionarLocais from './components/locais/Local';
import Faturas from './components/faturas/Faturas';
import AdicionarFaturas from './components/faturas/Fatura';
import Treinos from './components/treinos/Treinos';
import AdicionarTreinos from './components/treinos/Treino';
import Pessoas from './components/pessoas/Pessoas';
import AdicionarPessoas from './components/pessoas/Pessoa';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap';
import './custom.scss';
global.jQuery = require('jquery');
require('bootstrap');


ReactDOM.render(
<BrowserRouter>
  <Switch>
    <Route path="/" exact={true} component={App} />
    <Route path="/equipamentos/:id" component={AdicionarEquipamentos}/>
    <Route path="/equipamentos" component={Equipamentos} />  
    <Route path="/locais/:id" component={AdicionarLocais}/>
    <Route path="/locais" component={Locais} />
    <Route path="/faturas/:id" component={AdicionarFaturas}/>
    <Route path="/faturas" component={Faturas} />        
    <Route path="/treinos/:id" component={AdicionarTreinos}/>
    <Route path="/treinos" component={Treinos} />
    <Route path="/pessoas/:id" component={AdicionarPessoas}/>
    <Route path="/pessoas" component={Pessoas} />
  </Switch>
  </ BrowserRouter>,
  document.getElementById('root')
);